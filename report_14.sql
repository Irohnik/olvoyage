SELECt wagont.train_id, (d_station.city || ' - ' || a_station.city) as Train, SUM(wagon.nb_seat)-SUM(reservation_id) as "Nombre de sièges libres"
FROM t_wagon_train wagont
JOIN t_train train
    ON train.train_id = wagont.train_id
JOIN t_ticket ticket
    ON ticket.wag_tr_id = wagont.wag_tr_id
JOIN t_wagon wagon
    ON wagont.wagon_id = wagon.wagon_id
JOIN t_station d_station
    ON train.departure_station_id = d_station.station_id
JOIN t_station a_station
    ON train.arrival_station_id = a_station.station_id
WHERE train.distance > 300 AND train.arrival_time = '22/05/2019'
AND train.train_id IN (SELECT DISTINCT wagont.train_id
                        FROM t_ticket ticket
                        JOIN t_wagon_train wagont
                        ON ticket.wag_tr_id = wagont.wag_tr_id
                        WHERE ticket.reservation_id IS NOT NULL)
GROUP BY wagont.train_id, d_station.city, a_station.city
ORDER BY wagont.train_id;