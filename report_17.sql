UPDATE t_reservation
SET buy_method = INITCAP('&&TypeDePaiement'),
    price =
  (SELECT SUM((1 - (NVL(CASE
                        WHEN TO_CHAR(train.departure_time, 'D') BETWEEN 1 AND 5 THEN pass.discount_pct
                        ELSE pass.discount_we_pct
                    END, 0) / 100)) * (train.price + train.price * NVL(wagon.class_pct, 0) / 100)) "prix"
   FROM t_reservation reservation
   JOIN t_ticket ticket ON reservation.reservation_id = ticket.reservation_id
   JOIN t_wagon_train wg_train ON ticket.wag_tr_id = wg_train.wag_tr_id
   JOIN t_train train ON wg_train.train_id = train.train_id
   JOIN t_wagon wagon ON wg_train.wagon_id = wagon.wagon_id
   JOIN t_customer customer ON ticket.customer_id = customer.customer_id
   FULL OUTER JOIN t_pass pass ON customer.pass_id = pass.pass_id
   WHERE reservation.reservation_id = &&NumeroDeLaReservation
   GROUP BY reservation.reservation_id)
WHERE reservation_id = &NumeroDeLaReservation;