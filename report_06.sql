SELECT COUNT( DISTINCT t_employee.employee_id) AS "Nombre d'employes",
COUNT( DISTINCT t_customer.customer_id) AS "Nombre de clients",
ROUND (COUNT( DISTINCT pass.pass_id)/( COUNT( DISTINCT t_customer.customer_id))*100, 1) || '%' AS "Poucentage de clients abonnes",
COUNT( DISTINCT t_train.train_id) AS "Nombre de trains",
COUNT( DISTINCT t_station.station_id) AS "Nombre de gares",
COUNT( DISTINCT t_reservation.reservation_id) AS "Nombre de reservations",
COUNT( DISTINCT t_ticket.ticket_id) AS "Nombre de billets"
FROM t_employee employee
JOIN t_reservation reservation
  ON employee.employee_id = reservation.employee_id
JOIN t_customer customer
  ON reservation.buyer_id = customer.customer_id
JOIN (SELECT pass_id
      FROM t_customer
      WHERE ADD_MONTHS(pass_date, 12) > (SELECT Sysdate
      FROM dual)) pass
  ON pass.pass_id = customer.pass_id
JOIN t_ticket ticket
  ON reservation.reservation_id = ticket.reservation_id
JOIN t_wagon_train 
  ON t_ticket.wag_tr_id = t_wagon_train.wag_tr_id
JOIN t_train
  ON t_wagon_train.train_id = t_train.train_id
JOIN t_station
  ON t_train.departure_station_id = t_station.station_id;