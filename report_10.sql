SELECT COUNT(p.pass_name) AS "Client Senior"
FROM t_customer c
JOIN t_pass p
	ON c.pass_id = p.pass_id
JOIN t_reservation r
	ON c.customer_id = r.buyer_id
JOIN t_ticket t
	ON r.reservation_id = t.reservation_id
JOIN t_wagon_train w
	ON t.wag_tr_id = w.wag_tr_id
JOIN t_train tr
	ON w.train_id = tr.train_id
WHERE LOWER(p.pass_name) = 'senior'
	AND tr.departure_time LIKE '%/03/19'
	AND r.buy_method IS NOT NULL;