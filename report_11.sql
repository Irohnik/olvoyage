SELECT DISTINCT p.pass_name AS "Titre", w.train_id AS "Num�ro", sd.city || ' - ' || sa.city AS "Trajet", tr.price "Prix", (tr.price -(tr.price *(0.01 * p.discount_pct))) "r�du semaine", (tr.price-(tr.price *(0.01 * p.discount_we_pct))) "r�du WE"
FROM t_train tr
JOIN t_wagon_train w
   ON w.train_id = tr.train_id
JOIN t_station sd
   ON tr.departure_station_id = sd.station_id
JOIN t_station sa
   ON tr.arrival_station_id = sa.station_id
CROSS JOIN t_pass p
WHERE LOWER(sd.city) = 'paris'
ORDER BY w.train_id;