SELECT reservation.reservation_id AS "Reservation", reservation.creation_date AS "Creation date",
     employee.last_name || ' ' || employee.first_name AS "Employee",
     customer.last_name || ' ' || customer.first_name AS "Buyer"
FROM t_reservation reservation
JOIN t_employee employee
     ON reservation.employee_id = employee.employee_id
JOIN t_customer customer
     ON customer.customer_id = reservation.buyer_id
WHERE reservation.creation_date = (SELECT MIN(creation_date)
                         FROM t_reservation);