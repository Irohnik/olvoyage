SELECT DISTINCT(customer.last_name || ' ' || customer.first_name) AS "Customer"
FROM t_customer customer
JOIN t_reservation reservation
    ON customer.customer_id = reservation.buyer_id
JOIN t_ticket ticket
    ON reservation.reservation_id = ticket.reservation_id
WHERE ticket.customer_id <> reservation.buyer_id
ORDER BY "Customer";