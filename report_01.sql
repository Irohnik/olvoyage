SELECT DISTINCT last_name as "NOM"
FROM t_employee
WHERE employee_id = (SELECT employee_id
                     FROM t_reservation
                     GROUP BY employee_id
                     HAVING COUNT(reservation_id) = (SELECT MAX(COUNT(reservation_id))
                                                     FROM t_reservation
                                                     GROUP BY employee_id));