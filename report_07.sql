SELECT DISTINCT d_station.city || ' - ' || a_station.city AS "Train"
FROM t_ticket ticket
JOIN t_wagon_train w_train
    ON ticket.wag_tr_id = w_train.wag_tr_id
JOIN t_train train
    ON w_train.train_id = train.train_id
JOIN t_station d_station
    ON train.departure_station_id = d_station.station_id
JOIN t_station a_station
    ON train.arrival_station_id = a_station.station_id
WHERE train.train_id IN (SELECT *
                        FROM (SELECT train.train_id
                              FROM t_ticket ticket
                              JOIN t_wagon_train w_train
                                  ON ticket.wag_tr_id = w_train.wag_tr_id
                              JOIN t_train train
                                  ON w_train.train_id = train.train_id
                              GROUP BY train.train_id
                              ORDER BY COUNT(reservation_id) DESC)
                            WHERE rownum <= 5);