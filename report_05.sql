SELECT d_station.city AS "Station de d�part", TO_CHAR(train.departure_time, '(DD/MM/RR HH:MM)') AS "Heure de d�part" ,
  a_station.city AS "Station d'arriv�e", TO_CHAR(train.arrival_time, '(DD/MM/RR HH:MM)') AS "Heure d'arriv�e",
  train.distance AS "Distance parcourue", train.price AS "Prix"
FROM t_train train
JOIN t_station d_station
  ON train.departure_station_id = d_station.station_id
JOIN t_station a_station
  ON train.arrival_station_id = a_station.station_id
ORDER BY train_id;