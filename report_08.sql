SELECT first_name || ' ' || last_name AS "Clients sans abonnement", address "Adresse"
FROM t_customer
WHERE pass_id IS NULL
ORDER BY last_name, first_name;