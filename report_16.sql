SELECT last_name || ' ' || first_name as "Coordinateurs de ventes", salary+100 as "Nouveau Salaire"
FROM t_employee emp
JOIN (SELECT employee_id
     FROM t_employee
     WHERE manager_id IS NULL) chef
    ON emp.manager_id = chef.employee_id;