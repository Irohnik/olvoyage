SELECT reservation.reservation_id, customer.first_name || ' ' || customer.last_name as "Client",  d_station.city || ' - ' || a_station.city as "Train", departure_time
FROM t_customer customer 
JOIN t_reservation reservation
    ON reservation.buyer_id = customer.customer_id
JOIN t_ticket ticket
ON reservation.reservation_id = ticket.reservation_id
JOIN t_wagon_train wagont
    ON wagont.wag_tr_id = ticket.wag_tr_id
JOIN t_train train 
    ON train.train_id = wagont.train_id
JOIN t_station d_station
    ON train.departure_station_id = d_station.station_id
JOIN t_station a_station
    ON train.arrival_station_id = a_station.station_id
WHERE Trunc(Months_Between(sysdate, customer.birth_date)/12) < 25
AND reservation.creation_date < train.departure_time-20
AND train.departure_time BETWEEN'15/04/2019' AND '25/04/2019'
ORDER BY train.departure_time;