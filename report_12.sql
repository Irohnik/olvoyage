SELECT t_pass.pass_name "Titre d'abonnement", COUNT(t_customer.customer_id) "Nombre d'abonnement"
FROM t_pass
JOIN t_customer
	ON t_pass.pass_id = t_customer.pass_id
GROUP BY t_pass.pass_name
ORDER BY "Nombre d'abonnement" DESC;
