SELECT LPAD((t_employee.last_name || ' ' || t_employee.first_name),LENGTH(t_employee.last_name || ' ' || t_employee.first_name) +(LEVEL*2)-2, '_') "employee", COUNT(t_reservation.reservation_id) "Nombre r�servation"
FROM t_employee
JOIN t_reservation
  ON t_reservation.employee_id = t_employee.employee_id
WHERE t_reservation.employee_id = t_employee.employee_id
START WITH t_employee.manager_id = ( SELECT employee_id
FROM t_employee
WHERE manager_id IS NULL)
CONNECT BY PRIOR t_employee.employee_id = t_employee.manager_id
GROUP BY t_employee.last_name, t_employee.first_name, LEVEL;