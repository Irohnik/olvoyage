SELECT customer.last_name || ' ' || customer.first_name "Client",
CASE
  WHEN ADD_MONTHS(customer.pass_date, 12) < '15-05-2019' THEN 'Perime'
  WHEN customer.pass_date IS NULL THEN 'Aucun'
ELSE 'Valide'
END as "Etat abonnement",
pass.pass_name as "Abonnement"
FROM t_customer customer
JOIN t_pass pass
  ON customer.pass_id = pass.pass_id;