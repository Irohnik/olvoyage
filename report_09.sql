SELECT train_id AS "N°", (d_station.city || '-' || a_station.city) AS "Trajet", ROUND(distance / ((t.arrival_time - t.departure_time)*60)) || ' km/h' AS "Vitesse"
FROM t_train t
JOIN t_station d_station
        ON t.departure_station_id = d_station.station_id
JOIN t_station a_station
        ON t.arrival_station_id = a_station.station_id
ORDER BY train_id;